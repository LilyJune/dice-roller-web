
function roll (sides) {
  return Math.floor((Math.random() * sides) + 1)
}

function calculateMod (stat) {
  return Math.floor((stat - 10) / 2)
}

function rollStat () {
  const rolls = []
  let rollsStr = ''
  for (let i = 0; i < 4; i++) {
    const currentRoll = roll(6)
    rollsStr += currentRoll + ', '
    rolls.push(currentRoll)
  }
  rollsStr = rollsStr.slice(0, -2)

  const minVal = Math.min(...rolls)
  const sum = rolls.reduce((a, b) => a + b, 0)
  const finalVal = sum - minVal

  const mod = calculateMod(finalVal)
  const modStr = modifierToStr(mod)

  return `<div class='stat-container'>
            <div class='rolls-container'>
              <p class='rolls'>${rollsStr}</p>
            </div>
            <div class='mod-container'>
              <p class='modifier'>${modStr}</p>
            </div>
            <div class='sum-container'>
              <p class='ability'>${finalVal}</p>
            </div>
          </div>`
}

function modifierToStr (mod) {
  if (mod >= 0) {
    return `+${mod}`
  } else {
    return `${mod}`
  }
}

function rollStats () {
  $('#output-stats').empty()
  for (let i = 0; i < 6; i++) {
    $('#output-stats').prepend(rollStat)
  }
}

function submitDiceForm () {
  $('#output-stats').empty()
  const numDice = $('#numDice').val()
  const numSides = $('#sides').val()

  const rolls = []
  let rollsStr = ''
  for (let i = 0; i < numDice; i++) {
    const currentRoll = roll(numSides)
    rollsStr += currentRoll + ', '
    rolls.push(currentRoll)
  }
  rollsStr = rollsStr.slice(0, -2)
  const sum = rolls.reduce((a, b) => a + b, 0)

  $('#output-stats').prepend(`
    <div class='stat-container'>
      <div class='rolls-container'>
        <p class='rolls'>${rollsStr}</p>
      </div>
      <div class='mod-container'>
        <p class='modifier'>${sum}</p>
      </div>
    </div>
  `)
}
